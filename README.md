## CONFIGS :D

Basic config files to have a pleasant linux CLI experience. To get them all in
place, simply clone and run the INSTALL.sh script (old files will be renamed
with the extension ".bk").

