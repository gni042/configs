#!/bin/bash


# .bashrc
if [[ -f ~/.bashrc ]]; then
    echo "OLD ~/.bashrc MOVED TO ~/.bashrc.bk"
    mv ~/.bashrc ~/.bashrc.bk
fi

cp ./BASHRC ~/.bashrc

# tmux
if [[ -f ~/.tmux.conf ]]; then
    echo "OLD ~/.tmux.conf MOVED TO ~/.tmux.conf.bk"
    mv ~/.tmux.conf ~/.tmux.conf.bk
fi

cp ./TMUX ~/.tmux.conf

# neovim
if [[ -f ~/.config/nvim/init.vim ]]; then
    echo "OLD ~/.config/nvim/init.vim MOVED TO ~/.config/nvim/init.vim.bk"
    mv ~/.config/nvim/init.vim ~/.config/nvim/init.vim.bk
fi

mkdir -p ~/.config/nvim
cp ./NVIM ~/.config/nvim/init.vim


