set-window-option -g mode-keys vi
set-window-option -g aggressive-resize on

# Large history
set-option -g history-limit 500

# Escape time
set-option -sg escape-time 10

# y and p as in vim
unbind p
bind p paste-buffer
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'y' send -X copy-selection-and-cancel

# moving between windows with vim movement keys
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# moving between panes with vim movement keys
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# resize panes with vim movement keys
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# Reload config file
bind r source-file ~/.tmux.conf

# Pane border colours
setw -g mode-style bg=cyan
set -g pane-border-style fg=yellow
set -g pane-border-format bg=white
set -g pane-active-border-style fg=blue,bg=red

##############################################
# STATUS BAR                                 #
##############################################

#set-option -g status-utf8 on
set-option -g status-justify right
set-option -g status-bg black
set-option -g status-fg cyan
set-option -g status-interval 5
set-option -g status-left-length 30
set-option -g status-left '#[fg=magenta]» #[fg=blue,bold]#T#[default]'
set-option -g status-right '#[fg=red,bold][[ #(git branch) branch ]] #[fg=cyan]»» #[fg=blue,bold]###S #[fg=magenta]%R %m-%d#(acpi | cut -d ',' -f 2)#[default]'
set-option -g visual-activity on

#################################
# terminal window title as tmux #
#################################

set-option -g set-titles on
set-option -g set-titles-string "#S / #W"

